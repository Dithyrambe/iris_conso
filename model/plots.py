"""
This scripts output some plots for slides
3 figs should be rendered

1) Raw correlation matrix between variables
2) Same but with selected variables only
3) Scatter plot with 2 of the most correlated features with target variable 'conso'
"""

import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('data_set/train.csv')
data_reduce = pd.read_csv('data_set/train_reduce.csv')

# Plot correlation matrices with full & light datasets
for df in (data, data_reduce):
    relevant = df.drop(['Année', 'IRIS'], axis=1)  # remove non-relevant data
    relevant = relevant[['conso'] + [col for col in relevant.columns if col != 'conso']]  # workaround for having target
                                                                                          # variable in first row

    corr = relevant.corr()
    fig, ax = plt.subplots()
    cax = ax.matshow(corr)
    plt.xticks(range(len(corr.columns)), corr.columns, rotation='vertical')
    plt.yticks(range(len(corr.columns)), corr.columns)
    cbar = fig.colorbar(cax)
    cbar.set_label('correlation')


# Plot relation between most correlated features with target
filtered = data[
    (
        data['conso'] < data['conso'].quantile(0.8)) & (
        data['conso'] > data['conso'].quantile(0.2)) & (
        data['P14_RP'] < data['P14_RP'].quantile(0.8)) & (
        data['P14_RP'] > data['P14_RP'].quantile(0.2)) & (
        data['P14_NBPI_RPMAISON'] < data['P14_NBPI_RPMAISON'].quantile(0.8)) & (
        data['P14_NBPI_RPMAISON'] > data['P14_NBPI_RPMAISON'].quantile(0.2)
    )
    ]


ax = filtered.plot.scatter(x='P14_RP', y='P14_NBPI_RPMAISON', c='conso',
                           colormap='cool', alpha=0.25)
ax.set_xlabel('nb résidences principales')
ax.set_ylabel('nb pièces des résidences principales de type maison')
