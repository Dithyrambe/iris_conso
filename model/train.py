"""
This script trains some models and store them into the directory as pickle for reuse purpose.
"""

import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.externals import joblib
from sklearn.model_selection import GridSearchCV

GRID_SEARCH = False  # whether GRID_CV should be run

train = pd.read_csv('data_set/train.csv', dtype={'IRIS': str})
train = train.drop(['Année', 'IRIS'], axis=1)
train_reduce = pd.read_csv('data_set/train_reduce.csv', dtype={'IRIS': str})
train_reduce = train_reduce.drop(['Année', 'IRIS'], axis=1)

# Features for accurate predictor
y = train['conso'].values
X = train.drop('conso', axis=1).values

# Features for light predictor
y_reduce = train_reduce['conso'].values
X_reduce = train_reduce.drop('conso', axis=1).values

if GRID_SEARCH:
    # Perform grid search for the first run (greedy !)
    rf = RandomForestRegressor(n_jobs=-1)

    param_grid = {
        'n_estimators': [100, 200, 500],
        'max_features': [0.7, 'sqrt', 'log2'],
        'min_samples_leaf': [2, 5, 10]
    }

    CV_rf = GridSearchCV(estimator=rf, param_grid=param_grid, cv=5, verbose=1)
    CV_rf.fit(X, y)

    rf = RandomForestRegressor(
        n_jobs=-1,
        oob_score=True,
        **CV_rf.best_params_
    )

else:
    # Parameters are outputed from GRIDSEARCH
    rf = RandomForestRegressor(
        n_jobs=-1,
        n_estimators=500,
        max_features=0.7,
        min_samples_leaf=2,
        verbose=True,
        oob_score=True
    )

# Train predictor
rf.fit(X, y)

# Train light model for fast interpretation in app
light_model = RandomForestRegressor(
    n_jobs=-1,
    n_estimators=100,
    max_depth=10,
    max_features=0.7,
    oob_score=True,
    verbose=True
)

light_model.fit(X_reduce, y_reduce)

# Train a linear model for benchmark
lr = LinearRegression()
lr.fit(X, y)

# Store trained models as pickle
joblib.dump(rf, 'model/trained_regressor.pckl')
joblib.dump(light_model, 'model/trained_light_regressor.pckl')
joblib.dump(lr, 'model/trained_linear_model.pckl')
