"""
This scripts run all the preprocessing of raw datas and store training set (2011, 2012, 2013, 2014) and test set (2015)
"""

from model.data import Conso, Logements, Menages, Population, Centroids
from model.data_reduced import (
    Conso as RConso,
    Logements as RLogements,
    Menages as RMenages,
    Population as RPopulation,
    Centroids as RCentroids
)

TEST_YEAR = 2015

# Load Data
Conso.prepare()

# Get median values for target variable
median = Conso.data[
    Conso.data['Année'] < TEST_YEAR  # prevent bias
]['Conso totale secteur résidentiel (MWh)'].median()

type_iris_median = Conso.data[
    Conso.data['Année'] < TEST_YEAR  # prevent bias
].groupby('Type IRIS')[
    'Conso totale secteur résidentiel (MWh)'
].median().to_dict()


# Transform Type IRIS in median value for modality
@Conso.apply_to('Type IRIS')
@RConso.apply_to('Type IRIS')
def get_median(col):
    return col.apply(lambda x: type_iris_median.get(x, median))


Conso.prepare(rename=True)
Logements.prepare(rename=True)
Menages.prepare(rename=True)
Population.prepare(rename=True)
Centroids.prepare()

RConso.prepare(rename=True)
RLogements.prepare(rename=True)
RMenages.prepare(rename=True)
RPopulation.prepare(rename=True)
RCentroids.prepare()

df = Conso.data.merge(
    Logements.data,
    on='IRIS',
    how='left'
).merge(
    Menages.data,
    on='IRIS',
    how='left'
).merge(
    Population.data,
    on='IRIS',
    how='left'
).merge(
    Centroids.data,
    on='IRIS',
    how='left'
)

df_reduce = RConso.data.merge(
    RLogements.data,
    on='IRIS',
    how='left'
).merge(
    RMenages.data,
    on='IRIS',
    how='left'
).merge(
    RPopulation.data,
    on='IRIS',
    how='left'
).merge(
    RCentroids.data,
    on='IRIS',
    how='left'
)

# Drop all NA lines
df = df.dropna()
df_reduce = df_reduce.dropna()

train = df[df['Année'] < TEST_YEAR]
test = df[df['Année'] == TEST_YEAR]
train_reduce = df_reduce[df_reduce['Année'] < TEST_YEAR]
test_reduce = df_reduce[df_reduce['Année'] == TEST_YEAR]

train.to_csv('data_set/train.csv', index=False)
test.to_csv('data_set/test.csv', index=False)
train_reduce.to_csv('data_set/train_reduce.csv', index=False)
test_reduce.to_csv('data_set/test_reduce.csv', index=False)
