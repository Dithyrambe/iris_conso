from pymongo import MongoClient
from elasticsearch import Elasticsearch

from .. import app


mongo_client = MongoClient(
    app.config['MONGO_URL'],
    app.config['MONGO_PORT'],
    serverSelectionTimeoutMS=app.config['MAX_DELAY']
)

mongo = mongo_client[
    app.config['DATABASE_NAME']
]

es = Elasticsearch(hosts=[
    '{host}:{port}'.format(
        host=app.config['ELASTIC_URL'],
        port=app.config['ELASTIC_PORT']
    )
])
