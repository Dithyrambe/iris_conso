from tqdm import tqdm
import pandas as pd

from . import app

from . import es
from .models.geo import geo_levels, geo_levels_by_name
from .utils.geojson import open_geojson, check_geojson


def setup_es():
    if not es.indices.exists(app.config['ELASTIC_INDEX']):
        es.indices.create(index=app.config['ELASTIC_INDEX'])

    for level in geo_levels:
        geojson = open_geojson(level['data_location'])
        check_geojson(geojson, level['name'])

    for level in geo_levels:
        index_geo_ressource(level['name'])


def delete_index():
    es.indices.delete(app.config['ELASTIC_INDEX'])


def index_geo_ressource(geo_level_name):
    level = geo_levels_by_name.get(geo_level_name)

    assert level, "'{}' is not an available geo_level".format(geo_level_name)

    geojson = open_geojson(level['data_location'])
    resource = level.get('resource')
    resource.put_mapping()

    ids = [area['properties']['id'] for area in geojson['features']]
    bodies = [dict(
        **{prop: value for prop, value in area['properties'].items()},  # properties
        **{'geometry': area['geometry']},  # geometry
        **{'level_hierarchy': level['hierarchy']}
    ) for area in geojson['features']]

    if geo_level_name == 'iris':
        conso = pd.read_csv('data_set/test.csv', usecols=['IRIS', 'conso'], dtype={'IRIS': str}).set_index('IRIS')
        for body in bodies:
            body['conso'] = conso['conso'].get(body['id'], 0)

    for area in bodies:
        area['geometry']['type'] = area['geometry']['type'].lower()

    chunk_size = 10
    for success, info in tqdm(resource.bulk_index(ids, bodies, chunk_size=chunk_size, request_timeout=1000),
                              total=len(ids),
                              desc="Loading {}".format(geo_level_name)):
        if not success:
            pass


def get_conso_from_iris():
    for level_name, level in geo_levels_by_name.items():
        if level_name != 'iris':
            resource = level['resource']
            resource.aggregate('conso', from_type='iris')
