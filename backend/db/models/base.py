from bson import ObjectId
import multiprocessing as mp

from pymongo import MongoClient

from elasticsearch.helpers import scan, parallel_bulk


class MongoModel:
    _mongo = None
    _collection = None

    @classmethod
    def get(cls, _id):
        if isinstance(_id, str):
            _id = ObjectId(_id)
        return cls._collection.find_one({'_id': _id})

    @classmethod
    def find_one(cls, filter_=None):
        if filter_ is None:
            filter_ = None
        return cls._collection.find_one(filter_)

    @classmethod
    def find(cls, filter_=None):
        if filter_ is None:
            filter_ = {}
        return cls._collection.find(filter_)

    @classmethod
    def insert_one(cls, document):
        return cls._collection.insert_one(document)

    @classmethod
    def delete(cls, _id):
        if isinstance(_id, str):
            _id = ObjectId(_id)
        deleted = cls.get(_id)
        cls._collection.delete_one({'_id': ObjectId(_id)})
        return deleted


class ElasticModel:
    _es = None
    _index = None
    _doctype = None

    @classmethod
    def get(cls, _id):
        return cls._es.get(index=cls._index, doc_type=cls._doctype, id=_id)

    @classmethod
    def delete(cls, _id):
        deleted = cls.get(_id)
        cls._es.delete(index=cls._index, doc_type=cls._doctype, id=_id)
        return deleted

    @classmethod
    def index(cls, _id, body):
        cls._es.index(index=cls._index, doc_type=cls._doctype, id=_id, body=body)

    @classmethod
    def bulk_index(cls, ids, bodies, chunk_size=100, **kwargs):
        actions = ({
            '_index': cls._index,
            '_type': cls._doctype,
            '_id': _id,
            '_source': body
        } for _id, body in zip(ids, bodies))

        res = parallel_bulk(
            cls._es, actions, thread_count=mp.cpu_count(),
            chunk_size=chunk_size,
            **kwargs
        )

        return res

    @classmethod
    def scan(cls, query=None):

        if query is None:
            query = {
                "query": {
                    "bool": {
                        "must": {
                            "match_all": {}
                        }
                    }
                }
            }

        res = scan(
            cls._es,
            index=cls._index,
            doc_type=cls._doctype,
            query=query
        )

        return res
