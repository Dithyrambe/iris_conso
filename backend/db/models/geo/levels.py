from . import app

from ... import es
from .base import GeoElasticModel

__all__ = [
    'Region',
    'Department',
    'Commune',
    'Iris',
]


class Region(GeoElasticModel):
    _es = es
    _index = app.config['ELASTIC_INDEX']
    _doctype = "region"


class Department(GeoElasticModel):
    _es = es
    _index = app.config['ELASTIC_INDEX']
    _doctype = "dept"


class Commune(GeoElasticModel):
    _es = es
    _index = app.config['ELASTIC_INDEX']
    _doctype = "commune"


class Iris(GeoElasticModel):
    _es = es
    _index = app.config['ELASTIC_INDEX']
    _doctype = 'iris'
