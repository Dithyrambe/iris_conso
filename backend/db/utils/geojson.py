import json


def open_geojson(path_to_geojson):
    with open(path_to_geojson, 'r') as f:
        geojson = json.load(f)
    return geojson


def check_geojson(geojson, name=''):
    for key in ['type', 'features']:
        assert key in geojson, "{} geojson needs a '{}' key".format(name, key)

    for key in ['geometry', 'properties']:
        assert all((key in elt for elt in geojson['features'])), "{} geojson needs a '{}' key".format(name, key)

    for key in ['id', 'name']:
        assert all(
            (key in elt['properties'] for elt in geojson['features'])), "{} geojson needs a '{}' key".format(name, key)
