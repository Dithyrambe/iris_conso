from contextlib import redirect_stderr
from functools import wraps
import os

from pymongo.errors import ServerSelectionTimeoutError

from ..db import es, mongo_client


def nostderr(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        with open(os.devnull, 'w') as stderr, redirect_stderr(stderr):
            res = f(*args, **kwargs)
        return res
    return wrapper


@nostderr
def check_es_connection():
            return es.ping()


def check_mongo_connection():
    try:
        mongo_client.server_info()
        return True
    except ServerSelectionTimeoutError:
        return False

