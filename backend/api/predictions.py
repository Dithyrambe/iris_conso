from flask import Blueprint
import pandas as pd
from sklearn.externals import joblib
from treeinterpreter import treeinterpreter as ti

from .utils.decorator import mongojson

data_test = pd.read_csv('data_set/test.csv', dtype={'IRIS': str})
data_test = data_test.drop('Année', axis=1).set_index('IRIS')
data_test_reduce = pd.read_csv('data_set/test_reduce.csv', dtype={'IRIS': str})
data_test_reduce = data_test_reduce.drop('Année', axis=1).set_index('IRIS')

features = data_test.drop('conso', axis=1)
features_reduce = data_test_reduce.drop('conso', axis=1)
target = data_test['conso']

regressor = joblib.load('model/trained_regressor.pckl')
light_regressor = joblib.load('model/trained_light_regressor.pckl')

model_ep = Blueprint('model_ep', __name__, url_prefix='/api/model')


@model_ep.route('/<string:iris>')
@mongojson
def get_prediction(iris):
    X = features.loc[iris].values.reshape((1, features.shape[1]))
    X_reduce = features_reduce.loc[iris].values.reshape((1, features_reduce.shape[1]))
    observed = target.loc[iris]
    predicted = regressor.predict(X)[0]

    _, _, contrib = ti.predict(light_regressor, X_reduce)

    return {
        'observed': observed,
        'predict': predicted,
        'contrib': {
            'labels': list(features_reduce.columns[contrib[0].argsort()[::-1]]),
            'effects': list(contrib[0][contrib[0].argsort()[::-1]]),
        }
    }
