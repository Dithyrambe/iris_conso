from flask import Blueprint, abort

from ..db.models.geo import geo_levels_by_name

from .utils.decorator import mongojson, leafletize

geo_ep = Blueprint('geo_ep', __name__, url_prefix='/api/geo')


@geo_ep.route('/<string:geo_level>/<string:geo_id>', methods=['GET'])
@mongojson
@leafletize
def get_area(geo_level, geo_id):
    level = geo_levels_by_name.get(geo_level)

    if not level:
        abort(404)

    else:
        resource = level['resource']
        res = resource.get(geo_id)['_source']
        return res


@geo_ep.route('/<string:geo_level>', methods=['GET'])
@mongojson
@leafletize
def get_area_list(geo_level):
    level = geo_levels_by_name.get(geo_level)

    if not level:
        abort(404)

    else:
        resource = level['resource']
        res = [hit['_source'] for hit in resource.scan()]
        return {'geo_data': res}


@geo_ep.route('/<string:geo_level_to_search>/intersect_with/<string:geo_level_to_intersect>/<string:intersect_id>')
@mongojson
@leafletize
def get_intersection(geo_level_to_search, geo_level_to_intersect, intersect_id):
    search_level = geo_levels_by_name.get(geo_level_to_search)

    if not search_level:
        abort(404)

    else:
        resource = search_level['resource']

        hits = resource.get_intersect_with(doc_type=geo_level_to_intersect, _id=intersect_id)

        res = [hit['_source'] for hit in hits]
        return {'geo_data': res}



