#!/usr/bin/env bash

/usr/bin/env python model/preprocessing.py
/usr/bin/env python model/train.py
/usr/bin/env python model/evaluate.py
